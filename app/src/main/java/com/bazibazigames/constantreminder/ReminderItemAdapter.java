package com.bazibazigames.constantreminder;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by admin on 1/21/2017.
 */

public class ReminderItemAdapter extends ArrayAdapter<ReminderItem> {
    MyAudioRecord audio_recorder;

    public ReminderItemAdapter(Context context, List<ReminderItem> objects) {
        super(context, 0, objects);

        String recordingDirectory = Environment.getExternalStorageDirectory().getAbsolutePath() +  "/constant_reminder";
        String mFileName = "/Reminder_";
        audio_recorder = new MyAudioRecord(getContext(), mFileName, recordingDirectory);

    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if ( position == getCount()-1)
            return 1;
        return 0;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View listItemView = convertView;
        int itemType = getItemViewType(position);
        final ViewHolder mHolder;
        final ReminderItem item = getItem(position);

        TextView reminderName = null;
        ImageView editReminder = null;
        CheckBox checkBoxReminderItem = null;

        if (listItemView == null) {
            if (itemType ==0) {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                listItemView = inflater.inflate(R.layout.reminder_list_item, parent, false);


                reminderName = (TextView) listItemView.findViewById(R.id.textViewReminderName);
                reminderName.setText(item.getReminderName());

                checkBoxReminderItem = (CheckBox) listItemView.findViewById(R.id.checkBoxSelectReminder);

                editReminder = (ImageView) listItemView.findViewById(R.id.imageViewEditReminder);


                mHolder = new ViewHolder();
                mHolder.mTextView = reminderName;
                mHolder.mImageView = editReminder;
                mHolder.mCheckBox = checkBoxReminderItem;

                listItemView.setTag(mHolder);
            }

            else if (itemType == 1) { // If we're on the last position we don't have a regular item
                LayoutInflater inflater = LayoutInflater.from(getContext());
                listItemView = inflater.inflate(R.layout.reminder_list_new_item, parent, false);
            }
        } else{
            if (itemType == 0) {
                mHolder = (ViewHolder) listItemView.getTag();
                reminderName = mHolder.mTextView;
                editReminder = mHolder.mImageView;
                checkBoxReminderItem = mHolder.mCheckBox;
            }
        }

        if (itemType == 0){

            // Set the listener for reminder edit button
            editReminder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.v("New Item = ", "EDIT CLICKED");
                    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = inflater.inflate(R.layout.record_reminder_popup, null); //custom_layout is your xml file which contains popuplayout

                    final EditText myEditText = (EditText) view.findViewById(R.id.editTextNewReminderName);
                    myEditText.setText(item.getReminderName());

                    final PopupWindow popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    if (Build.VERSION.SDK_INT >= 21) {
                        popupWindow.setElevation(5.0f);
                    }

                    popupWindow.setFocusable(true);
                    popupWindow.update();


                    ImageView imageViewRecord = (ImageView) view.findViewById(R.id.imageViewRecord);
                    imageViewRecord.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_DOWN){
                                audio_recorder.startRecording();
                            }
                            if (event.getAction() == MotionEvent.ACTION_UP) {
                                audio_recorder.stopRecording();
                            }

                            item.setReminderAudioLocation( audio_recorder.recordingDestination);

                            return false;
                        }
                    });
                    Button dismissButton = (Button) view.findViewById(R.id.buttonDismissPopup);
                    dismissButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            item.setReminderName(myEditText.getText().toString());

                            popupWindow.dismiss();
                        }
                    });

                    popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
                }
            });
            //

            checkBoxReminderItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    item.setSelected(isChecked);
                }
            });

            ImageView imageViewRemoveReminder = (ImageView) listItemView.findViewById(R.id.imageViewRemoveReminder);

            imageViewRemoveReminder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ( (MainActivity)getContext()).removeReminderItem(position);
                }
            });
            reminderName.setText(item.getReminderName());
            checkBoxReminderItem.setChecked( item.getSelected());
        }

        return listItemView;
    }

    private class ViewHolder{
        private TextView mTextView;
        private ImageView mImageView;
        private CheckBox mCheckBox;

    }
}
