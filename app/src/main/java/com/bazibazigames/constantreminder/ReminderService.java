package com.bazibazigames.constantreminder;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Icon;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaDataSource;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by admin on 2/18/2017.
 */

public class ReminderService extends IntentService {

    ReminderManager rm;
    MyApplication settings;

    private ArrayList<ReminderItem> mReminders;
    private Context mContext;
    private Timer mtimer;
    private int mInterval;
    private AudioManager am;
    private MediaPlayer myPlayer;

    public boolean isRunning = false;
    AudioManager.OnAudioFocusChangeListener afChangeListener;

    private String LOG_TAG = "ReminderServiceTag";

    public ReminderService() {
        super("ReminderService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        settings = ((MyApplication) getApplication());
        rm = settings.reminderManager;
        mReminders = new ArrayList<ReminderItem>();

        mContext = getApplicationContext();
        mtimer = new Timer();
        mInterval = settings.reminderInterval; // in seconds

        addReminders(settings.remindersList);
        am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
            @Override
            public void onAudioFocusChange(int focusChange) {

            }
        };
        myPlayer = new MediaPlayer();

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification = new Notification.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_constant_reminder))
                .setContentTitle("Constant Reminder is running in the background")
                .setContentText("You're being constantly reminded")
                .setSmallIcon(R.drawable.ic_cr_notification)
                .setContentIntent(pendingIntent)
                .setTicker("Constant Reminder is running in the background")
                .build();


        startForeground(1337, notification);


        isRunning = true;
        while (isRunning) {
            playRandomReminder();
            try {
                Thread.sleep(mInterval * 1000);
            } catch (Exception e) {
                Log.v(LOG_TAG, "sleep exception");
            }
        }


    }

    public void addReminders(ArrayList<ReminderItem> newReminders) {
        // I'm only including the items that have been selected
        // The dummy content also gets excluded because it is never selected
        for (int i = 0; i < newReminders.size(); i++) {
            if (newReminders.get(i).getSelected() == true)
                mReminders.add(newReminders.get(i));
        }
    }

    public void startReminding() {
        isRunning = true;

        mtimer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                playRandomReminder();
            }
        };
        mtimer.schedule(task, 1000, mInterval * 1000);
    }

    public void stopReminding() {
        isRunning = false;
        mtimer.cancel();
    }

    private void playRandomReminder() {
        // Play the Audio File of that reminder
        // Request audio focus for playback
        int result = am.requestAudioFocus(afChangeListener,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            // Start playback


            Random rand = new Random();
            Log.v("mReminders.size() = ", mReminders.size() + "");
            if (mReminders.size() > 0) {
                int n = rand.nextInt(mReminders.size());
                Log.v(LOG_TAG, "Current Reminder = " +  mReminders.get(n).getReminderName());
                Log.v(LOG_TAG, "Reminder Location = " + mReminders.get(n).getReminderAudioLocation());
                if (myPlayer == null)
                    myPlayer = new MediaPlayer();

                try {
                    Log.v(LOG_TAG, "PlayRandomReminder = " + mReminders.get(n).getReminderName());
                    String filePath = mReminders.get(n).getReminderAudioLocation();

                    myPlayer.reset();
                    myPlayer.setDataSource(filePath);
                    myPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    myPlayer.prepare();
                    myPlayer.start();


                } catch (Exception e) {
                    Log.v(LOG_TAG, "exception = ", e);
                }


            }

            am.abandonAudioFocus(afChangeListener);
        }
    }

    @Override
    public void onDestroy() {
        stopReminding();
        super.onDestroy();
    }

}