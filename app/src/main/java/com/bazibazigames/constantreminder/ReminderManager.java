package com.bazibazigames.constantreminder;

import android.content.Context;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.util.Log;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by admin on 1/25/2017.
 */

public class ReminderManager {
    private ArrayList<ReminderItem> mReminders;
    private Context mContext;
    private Timer mtimer;
    private int mInterval;
    private AudioManager am;


    public boolean isRunning = false;

    AudioManager.OnAudioFocusChangeListener afChangeListener;

    public ReminderManager(Context context, int interval){
        mReminders = new ArrayList<ReminderItem>();
        mContext = context;
        mtimer = new Timer();
        mInterval = interval; // in seconds

        am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
            @Override
            public void onAudioFocusChange(int focusChange) {

            }
        };

    }

    public ArrayList<ReminderItem> getReminders(){return mReminders;}
    public void addReminders(ArrayList<ReminderItem> newReminders){
        // I'm only including the items that have been selected
        // The dummy content also gets excluded because it is never selected
        for (int i=0; i<newReminders.size(); i++) {
            if (newReminders.get(i).getSelected() == true)
                mReminders.add(newReminders.get(i));
        }
    }

    public void startReminding(){
        isRunning = true;

        mtimer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                playRandomReminder();
            }
        };
        mtimer.schedule(task,1000, mInterval * 1000);
    }

    public void stopReminding(){
        isRunning = false;
        mtimer.cancel();
    }

    public void clear(){
        mReminders.clear();
    }

    private void playRandomReminder() {
        // TO DO:
        //      Play the Audio File of that reminder


// Request audio focus for playback
        int result = am.requestAudioFocus(afChangeListener,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            // Start playback


            Random rand = new Random();
            Log.v("mReminders.size() = ", mReminders.size() + "");
            if (mReminders.size() > 0) {
                int n = rand.nextInt(mReminders.size());
                Log.v("Current Reminder = ", mReminders.get(n).getReminderName());
                Log.v("Reminder Location = ", mReminders.get(n).getReminderAudioLocation());

                try {
//                    MyAudioRecord.play(mReminders.get(n).getReminderAudioLocation());
                } catch (Exception e) {
                }
            }

            am.abandonAudioFocus(afChangeListener);
        }
    }

}
