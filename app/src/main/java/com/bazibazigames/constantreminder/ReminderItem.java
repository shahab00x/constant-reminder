package com.bazibazigames.constantreminder;

/**
 * Created by admin on 1/21/2017.
 */

public class ReminderItem {
    private String mReminderName;
    private String mReminderAudioLocation;
    private boolean mSelected;
    private long id = -1;

    public ReminderItem(String reminderName, String reminderAudioLocation){
        mReminderAudioLocation = reminderAudioLocation;
        mReminderName = reminderName;
        mSelected = false;
    }

    public String getReminderName() {
        return mReminderName;
    }
    public void setReminderName(String reminderName){mReminderName = reminderName;}

    public String getReminderAudioLocation() {
        return mReminderAudioLocation;
    }
    public void setReminderAudioLocation(String reminderAudioLocation){ mReminderAudioLocation = reminderAudioLocation;}
    public boolean getSelected(){return mSelected;}
    public void setSelected(boolean selected){mSelected = selected;}

    public long getId(){return id;}
    public void setId(long newId){id = newId;}
}
