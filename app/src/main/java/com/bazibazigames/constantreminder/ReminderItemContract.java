package com.bazibazigames.constantreminder;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by admin on 1/27/2017.
 */

public final class ReminderItemContract {
    private ReminderItemContract() {}

    public static class ReminderItemEntry implements BaseColumns{
        /* Table name for our reminders */
        public static final String TABLE_NAME = "reminders";

        /* Type: INTEGER*/
        public final static String _ID = BaseColumns._ID;

        /* Type: TEXT*/
        public static final String COLUMN_NAME_REMINDER_NAME = "name";

        /* Type: TEXT*/
        public static final String COLUMN_NAME_AUDIO_FILE_LOCATION = "audio_location";

        /* TYPE: INTEGER*/
        public static final String COLUMN_NAME_CHECKED = "checked";

        /* Possible values for reminder checkbox status*/
        public static final int REMINDER_CHECKED = 1;
        public static final int REMINDER_NOT_CHECKED = 0;


    }

}
