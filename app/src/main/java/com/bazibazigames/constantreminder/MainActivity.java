package com.bazibazigames.constantreminder;

import android.Manifest;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import com.bazibazigames.constantreminder.ReminderItemContract.ReminderItemEntry;

public class MainActivity extends AppCompatActivity {

    private ArrayList<ReminderItem> myReminders;
    private ListView listViewReminders;
    private ReminderItemAdapter remindersListAdapter;

    private SharedPreferences stored_data;
    private ReminderManager reminderManager;
    private Button buttonStartReminding;
    private ReminderItemDbHelper itemDbHelper;
    private Intent reminderService;
    private MyApplication settings;
    private MyAudioRecord audio_recorder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        settings = ((MyApplication) getApplication());

        settings.reminderInterval = 43;

        itemDbHelper = new ReminderItemDbHelper(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 0);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }

        String recordingDirectory = Environment.getExternalStorageDirectory().getAbsolutePath() + "/constant_reminder";
        String mFileName = "/Reminder_";
        audio_recorder = new MyAudioRecord(this, mFileName, recordingDirectory);

        reminderManager = new ReminderManager(this, settings.reminderInterval);

        buttonStartReminding = (Button) findViewById(R.id.buttonStartReminders);

        stored_data = this.getSharedPreferences("constant_reminder_app_data", 0);

        reminderService = new Intent(this, ReminderService.class);
        stopService(reminderService);
    }

    @Override
    protected void onStart() {
        super.onStart();

        myReminders = readFromDb();

        myReminders.add(myReminders.size(), new ReminderItem("Dummy Content", ""));

        remindersListAdapter = new ReminderItemAdapter(this, myReminders);

        listViewReminders = (ListView) findViewById(R.id.listViewReminders);
        listViewReminders.setAdapter(remindersListAdapter);

    }

    @Override
    protected void onStop() {
        super.onStop();

        for (int i = 0; i < myReminders.size() - 1; i++) {
            insertReminderIntoDb(myReminders.get(i));
        }

    }

    public void onAddNewReminderClick(View v) {
        Log.v("New Item = ", "CLICKED");
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.record_reminder_popup, null); //custom_layout is your xml file which contains popuplayout

        final PopupWindow popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        if (Build.VERSION.SDK_INT >= 21) {
            popupWindow.setElevation(5.0f);
        }

        popupWindow.setFocusable(true);
        popupWindow.update();

        final EditText myEditText = (EditText) view.findViewById(R.id.editTextNewReminderName);

        ImageView imageViewRecord = (ImageView) view.findViewById(R.id.imageViewRecord);
        imageViewRecord.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    audio_recorder.startRecording();
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    audio_recorder.stopRecording();
                }

                // Can't assign directly, because java is a crappy language
//                recordingFilePath.concat(audio_recorder.recordingDestination);

                return false;
            }
        });

        Button dismissButton = (Button) view.findViewById(R.id.buttonDismissPopup);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reminderName = myEditText.getText().toString();
                myReminders.add(myReminders.size() - 1, new ReminderItem(reminderName, audio_recorder.recordingDestination));
                remindersListAdapter.notifyDataSetChanged();
                popupWindow.dismiss();
            }
        });

        popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
    }


    public void editReminderClick(View v) {
        Log.v("New Item = ", "EDIT CLICKED");
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.record_reminder_popup, null); //custom_layout is your xml file which contains popuplayout

        final PopupWindow popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        if (Build.VERSION.SDK_INT >= 21) {
            popupWindow.setElevation(5.0f);
        }

        popupWindow.setFocusable(true);
        popupWindow.update();

        final EditText myEditText = (EditText) view.findViewById(R.id.editTextNewReminderName);

        Button dismissButton = (Button) view.findViewById(R.id.buttonDismissPopup);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reminderName = myEditText.getText().toString();
                myReminders.add(myReminders.size() - 1, new ReminderItem(reminderName, ""));
                remindersListAdapter.notifyDataSetChanged();
                popupWindow.dismiss();
            }
        });

        popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
    }

    public void removeReminderItem(int reminderIndex) {
        // First remove the reminder from the database if it exists
        SQLiteDatabase db = itemDbHelper.getWritableDatabase();
        if (myReminders.get(reminderIndex).getId() != -1)
            try {
                db.delete(ReminderItemEntry.TABLE_NAME, ReminderItemEntry._ID + "=" + myReminders.get(reminderIndex).getId(), null);
            } catch (Exception e) {
            }

        // Now remove it from the list
        myReminders.remove(reminderIndex);
        remindersListAdapter.notifyDataSetChanged();
    }

    public void startStopReminding(View v) {
        if (buttonStartReminding.getText().equals("Start Reminding"))
            startReminding();
        else if (buttonStartReminding.getText().equals("Stop Reminding"))
            stopReminding();

    }

    private void startReminding() {
        reminderManager.clear();
        reminderManager.addReminders(myReminders);
        settings.remindersList = myReminders;
        settings.reminderManager = reminderManager;

        startService(reminderService);



        buttonStartReminding.setText("Stop Reminding");
    }

    private void stopReminding() {
        stopService(reminderService);
//        reminderManager.stopReminding();

        buttonStartReminding.setText("Start Reminding");
    }

    private ArrayList<ReminderItem> readFromDb() {
        SQLiteDatabase db = itemDbHelper.getReadableDatabase();
        ArrayList<ReminderItem> retrievedReminderItems = new ArrayList<ReminderItem>();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                ReminderItemEntry._ID,
                ReminderItemEntry.COLUMN_NAME_REMINDER_NAME,
                ReminderItemEntry.COLUMN_NAME_AUDIO_FILE_LOCATION,
                ReminderItemEntry.COLUMN_NAME_CHECKED};

        // Perform a query on the pets table
        Cursor cursor = db.query(
                ReminderItemEntry.TABLE_NAME,   // The table to query
                projection,            // The columns to return
                null,                  // The columns for the WHERE clause
                null,                  // The values for the WHERE clause
                null,                  // Don't group the rows
                null,                  // Don't filter by row groups
                null);                   // The sort order

        try {

            // Figure out the index of each column
            int idColumnIndex = cursor.getColumnIndex(ReminderItemEntry._ID);
            int nameColumnIndex = cursor.getColumnIndex(ReminderItemEntry.COLUMN_NAME_REMINDER_NAME);
            int audioFileColumnIndex = cursor.getColumnIndex(ReminderItemEntry.COLUMN_NAME_AUDIO_FILE_LOCATION);
            int checkBoxColumnIndex = cursor.getColumnIndex(ReminderItemEntry.COLUMN_NAME_CHECKED);

            Log.v("MYDB", "Working");
            // Iterate through all the returned rows in the cursor
            while (cursor.moveToNext()) {
                // Use that index to extract the String or Int value of the word
                // at the current row the cursor is on.
                int currentID = cursor.getInt(idColumnIndex);
                String currentName = cursor.getString(nameColumnIndex);
                String currentAudioFile = cursor.getString(audioFileColumnIndex);
                int currentCheckedStatus = cursor.getInt(checkBoxColumnIndex);

                ReminderItem r = new ReminderItem(currentName, currentAudioFile);
                r.setSelected(currentCheckedStatus == ReminderItemEntry.REMINDER_CHECKED);
                r.setId(currentID);

                retrievedReminderItems.add(r);

            }
        } catch (Exception e) {
            Log.v("MY DB", "Not Working");
        } finally {
            // Always close the cursor when you're done reading from it. This releases all its
            // resources and makes it invalid.
            cursor.close();
        }

        return retrievedReminderItems;
    }

    private void insertReminderIntoDb(ReminderItem reminderItem) {
        // Gets the database in write mode
        SQLiteDatabase db = itemDbHelper.getWritableDatabase();

        // Create a ContentValues object where column names are the keys,
        // and Toto's pet attributes are the values.
        ContentValues values = new ContentValues();
        String strFilter = "";

        values.put(ReminderItemEntry.COLUMN_NAME_REMINDER_NAME, reminderItem.getReminderName());
        values.put(ReminderItemEntry.COLUMN_NAME_AUDIO_FILE_LOCATION, reminderItem.getReminderAudioLocation());
        values.put(ReminderItemEntry.COLUMN_NAME_CHECKED, reminderItem.getSelected());

        // Insert a new row for Toto in the database, returning the ID of that new row.
        // The first argument for db.insert() is the pets table name.
        // The second argument provides the name of a column in which the framework
        // can insert NULL in the event that the ContentValues is empty (if
        // this is set to "null", then the framework will not insert a row when
        // there are no values).
        // The third argument is the ContentValues object containing the info for Toto.
        if (reminderItem.getId() != -1) {
            db.update(ReminderItemEntry.TABLE_NAME, values, ReminderItemEntry._ID + "=" + reminderItem.getId(), null);
        } else {
            long newRowId = db.insert(ReminderItemEntry.TABLE_NAME, null, values);
        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }
}
