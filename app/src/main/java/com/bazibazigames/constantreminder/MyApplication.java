package com.bazibazigames.constantreminder;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

public class MyApplication extends Application {

    public ReminderManager reminderManager;
    public int reminderInterval = 30;
    public ArrayList<ReminderItem> remindersList;
}