package com.bazibazigames.constantreminder;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by admin on 12/7/2016.
 */
public class MyAudioRecord extends Activity {
    private static final String LOG_TAG = "AudioRecordTest";
    public static String mFileName = "test.mp3";
    private static String recordingDirectory;
    private MediaRecorder mRecorder = null;

    private boolean saveRecordings = true;

    private MediaPlayer mPlayer = null;
    private long last_recording_number = 0;
    private SharedPreferences settings;


    private static final int RECORDER_SAMPLERATE = 44100;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private AudioRecord recorder = null;
    private Thread recordingThread = null;
    public boolean isRecording = false;
    private int audio_source;
    private Context mContext;

    // The path of the last recorded file
    public String recordingDestination = "";

    public MyAudioRecord(Context context, String file_name, String recording_directory) {
        mContext = context;
        mFileName = file_name;
        recordingDirectory = recording_directory;
        File dir = new File(recording_directory);
        if (!dir.exists())
            dir.mkdir();
        settings = mContext.getSharedPreferences("my_voice_recorder_settings", 0);
        if (!settings.contains("last_file_number")) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putLong("last_file_number", 0);
            editor.commit();
        } else {
            last_recording_number = settings.getLong("last_file_number", 0);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setAudioSource("built-in"); // Set the audio source to be the microphone at the back of the phone

//        setButtonHandlers();
//        enableButtons(false);

        int bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE,
                RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);
    }


    public void setAudioSource(String source) {
        if (source.equalsIgnoreCase("built-in"))
            audio_source = MediaRecorder.AudioSource.CAMCORDER;
        else if (source.equalsIgnoreCase("external"))
            audio_source = MediaRecorder.AudioSource.MIC;
    }

    public void saveRecordings(boolean value) {
        saveRecordings = value;
    }

    public void toggleRecordAndPlayback() {
        toggleRecordAndPlayback(true, true);
    }

    public void toggleRecordAndPlayback(boolean playback, boolean turnScreenOff) {
        if (isRecording == false) {
            try {
                stopPlaying();
            } catch (Exception e) {
            }
            isRecording = true;
            startRecording();

//            if (turnScreenOff == true) {
//                // Turn the screen off
//                WindowManager.LayoutParams params = getWindow().getAttributes();
//                params.screenBrightness = -1;
//                getWindow().setAttributes(params);
//            }


        } else if (isRecording == true) {
            try {
                stopRecording();
            } catch (Exception e) {
            }
            if (playback) {
                startPlaying();
            }
            isRecording = false;
        }
    }

    public void playLastOne() {
        startPlaying();
    }

    private void startPlaying() {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(recordingDirectory + mFileName + ".wav");
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    public static void play( String filePath) {
        MediaPlayer myPlayer = new MediaPlayer();

        try {
            myPlayer.setDataSource(filePath);
            myPlayer.prepare();
            myPlayer.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        myPlayer.release();
        myPlayer = null;
    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }

    public void copy_file(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }


//    private void setButtonHandlers() {
//        ((Button) findViewById(R.id.buttonRecord)).setOnClickListener(btnClick);
//        ((Button) findViewById(R.id.btnStop)).setOnClickListener(btnClick);
//    }

    private void enableButton(int id, boolean isEnable) {
        ((Button) findViewById(id)).setEnabled(isEnable);
    }

//    private void enableButtons(boolean isRecording) {
//        enableButton(R.id.btnStart, !isRecording);
//        enableButton(R.id.btnStop, isRecording);
//    }

    int BufferElements2Rec = 1024; // want to play 2048 (2K) since 2 bytes we use only 1024
    int BytesPerElement = 2; // 2 bytes in 16bit format


    public void startRecording() {

        recorder = new AudioRecord(audio_source,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING, BufferElements2Rec * BytesPerElement);

        recorder.startRecording();
        isRecording = true;
        recordingThread = new Thread(new Runnable() {
            public void run() {
                writeAudioDataToFile();
            }
        }, "AudioRecorder Thread");
        recordingThread.start();

        Vibrator v = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(50);
    }

    //convert short to byte
    private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];
        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;

    }

    private void writeAudioDataToFile() {
        // Write the output audio in byte

        String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + mFileName;
        short sData[] = new short[BufferElements2Rec];

        FileOutputStream os = null;
        try {
            os = new FileOutputStream(filePath);
        } catch (FileNotFoundException e) {
            Log.v("WRITE PATH: ", filePath);
            e.printStackTrace();
        }

        while (isRecording) {
            // gets the voice output from microphone to byte format

            recorder.read(sData, 0, BufferElements2Rec);
            System.out.println("Short writing to file" + sData.toString());
            try {
                // // writes the data to file from buffer
                // // stores the voice buffer
                byte bData[] = short2byte(sData);
                os.write(bData, 0, BufferElements2Rec * BytesPerElement);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            os.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void stopRecording() {
        // stops the recording activity
        if (null != recorder) {
            isRecording = false;
            recorder.stop();
            recorder.release();
            recorder = null;
            recordingThread = null;

            File src = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + mFileName);
            File dst = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + mFileName + ".wav");

            try {
                rawToWave(src, dst);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (saveRecordings) {
                File src2 = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + mFileName + ".wav");
                recordingDestination = recordingDirectory + mFileName + Long.toString(last_recording_number) + ".wav";
                File dst2 = new File(recordingDestination);

                try {
                    copy_file(src2, dst2);
                } catch (IOException e) {
                }

                last_recording_number += 1;

                // Save the number of last recorded audio file to shared preferences
                SharedPreferences.Editor editor = settings.edit();
                editor.putLong("last_file_number", last_recording_number);
                editor.commit();
            }
        }
    }

    private void rawToWave(final File rawFile, final File waveFile) throws IOException {

        byte[] rawData = new byte[(int) rawFile.length()];
        DataInputStream input = null;
        try {
            input = new DataInputStream(new FileInputStream(rawFile));
            input.read(rawData);
        } finally {
            if (input != null) {
                input.close();
            }
        }

        DataOutputStream output = null;
        try {
            output = new DataOutputStream(new FileOutputStream(waveFile));
            // WAVE header
            // see http://ccrma.stanford.edu/courses/422/projects/WaveFormat/
            writeString(output, "RIFF"); // chunk id
            writeInt(output, 36 + rawData.length); // chunk size
            writeString(output, "WAVE"); // format
            writeString(output, "fmt "); // subchunk 1 id
            writeInt(output, 16); // subchunk 1 size
            writeShort(output, (short) 1); // audio format (1 = PCM)
            writeShort(output, (short) 1); // number of channels
            writeInt(output, 44100); // sample rate
            writeInt(output, RECORDER_SAMPLERATE * 2); // byte rate
            writeShort(output, (short) 2); // block align
            writeShort(output, (short) 16); // bits per sample
            writeString(output, "data"); // subchunk 2 id
            writeInt(output, rawData.length); // subchunk 2 size
            // Audio data (conversion big endian -> little endian)
            short[] shorts = new short[rawData.length / 2];
            ByteBuffer.wrap(rawData).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
            ByteBuffer bytes = ByteBuffer.allocate(shorts.length * 2);
            for (short s : shorts) {
                bytes.putShort(s);
            }

            output.write(fullyReadFileToBytes(rawFile));
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    byte[] fullyReadFileToBytes(File f) throws IOException {
        int size = (int) f.length();
        byte bytes[] = new byte[size];
        byte tmpBuff[] = new byte[size];
        FileInputStream fis = new FileInputStream(f);
        try {

            int read = fis.read(bytes, 0, size);
            if (read < size) {
                int remain = size - read;
                while (remain > 0) {
                    read = fis.read(tmpBuff, 0, remain);
                    System.arraycopy(tmpBuff, 0, bytes, size - remain, read);
                    remain -= read;
                }
            }
        } catch (IOException e) {
            throw e;
        } finally {
            fis.close();
        }

        return bytes;
    }

    private void writeInt(final DataOutputStream output, final int value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
        output.write(value >> 16);
        output.write(value >> 24);
    }

    private void writeShort(final DataOutputStream output, final short value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
    }

    private void writeString(final DataOutputStream output, final String value) throws IOException {
        for (int i = 0; i < value.length(); i++) {
            output.write(value.charAt(i));
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
