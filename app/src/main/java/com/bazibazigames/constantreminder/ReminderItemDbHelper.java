package com.bazibazigames.constantreminder;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by admin on 1/27/2017.
 */

public class ReminderItemDbHelper extends SQLiteOpenHelper {
    public static final String LOG_TAG = ReminderItemDbHelper.class.getSimpleName();

    public static final String DATABASE_NAME = "ReminderItems.db";

    public static final int DATABASE_VERSION = 1;

    public ReminderItemDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + ReminderItemContract.ReminderItemEntry.TABLE_NAME + " (" +
                        ReminderItemContract.ReminderItemEntry._ID + " INTEGER PRIMARY KEY," +
                        ReminderItemContract.ReminderItemEntry.COLUMN_NAME_REMINDER_NAME + " TEXT," +
                        ReminderItemContract.ReminderItemEntry.COLUMN_NAME_CHECKED + " TEXT," +
                        ReminderItemContract.ReminderItemEntry.COLUMN_NAME_AUDIO_FILE_LOCATION + " TEXT)";

        db.execSQL(SQL_CREATE_ENTRIES);
    }


    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + ReminderItemContract.ReminderItemEntry.TABLE_NAME;

        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}

